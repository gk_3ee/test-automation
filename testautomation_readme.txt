######################Assignment Test Automation Javadoc###########################

/**Parameters required to execute the automation suite:
*testrun.java class is the main class: It calls all the function required for execution from rest of the implemented classes
*DashBoardPage onDashBordPage = onLoginPage.fillFormWithData("username", "password").getPage(); : Enter your usename and password
*HomePage.java: Conatins the function to go the the given test url
*IssuePage issueDetails = onDashBordPage.gotoIssueDetailPage("issue_id"): Enter the issue_id you want to update
*LoginPage.java: Contains login function
*DashBoardPage.java: Contains page that appears after successfull login and responsible for creating issue [Testcase 1]
*On DashBoardPage below function is responsible for creating an issue with the below listed parameter. 
 If any changes is required to create an issue that can be made below nowhere else.
*public DashBoardPage fillCreateTaskFrom() throws InterruptedException{                  /** Function to create issue */
		selectIssueType("bug");
		writeSummary("This is a test");
		selectSecurityType(2); /*In this case we are taking the index value*/
		selectPriority("blocker");
		selectComponentType("component-2");
		enterEnvironment("yes good");
		enterTimeForTask("1d");
		enterDescription("test automation for jira");
		createIssue();
		return new DashBoardPage(driver);
	}

*IssuePage.java: When you serach an issue with it's ticket id with the search option on the dashboard. It will take you the page
                 with issue details where we can update an issue
*  public IssuePage updateIssue(DashBoardPage update) throws InterruptedException{             /** function to update existing issue */
	update.writeSummary("This is a new feature");
	update.enterDescription("update test automation for jira");
	clickUpdateButton();                                                                   /** all function are reused from */
	Thread.sleep(5000);
	return new IssuePage(driver);
}

*JiraSearch.java: This page is reposnsible for execution of Jira search with the filter's in the below mentioned function
 public JiraSearch applyFilters() throws InterruptedException{          /** function to search via jira search */
		selectProjectFilter("tst");  
		setIssueTypeFilter("bug");
		setStatusTypeFilter("open");
		selectAssignTypeFilter("unassigned");
		driver.findElement(By.xpath("//*[@id='content']/div[1]/div[4]/div/form/div[1]/div[1]/div[1]/div[1]/div/div[1]/ul/li[7]/button")).click();
		return new JiraSearch(driver);
	}

*/

