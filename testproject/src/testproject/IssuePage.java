package testproject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class IssuePage  extends AbstractPage {
       
	public IssuePage(WebDriver driver) {
		super(driver);
   }

public IssuePage  clickEditIssue() throws InterruptedException{                            /** function to click on edit issue button */
	 driver.findElement(By.className("issueaction-edit-issue")).click();
	 Thread.sleep(20000);
	 return new IssuePage(driver);
}

public void clickUpdateButton() throws InterruptedException{                               /** function to click on update issue button */
	driver.findElement(By.id("edit-issue-submit")).click();
	Thread.sleep(6000);
}

public IssuePage updateIssue(DashBoardPage update) throws InterruptedException{             /** function to update existing issue */
	update.writeSummary("This is a new feature");
	update.enterDescription("update test automation for jira");
	clickUpdateButton();                                                                   /** all function are reused from */
	Thread.sleep(5000);
	return new IssuePage(driver);
}

}