package testproject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

    public class JiraSearch extends AbstractPage {
    
	public JiraSearch(WebDriver driver) {
		super(driver);
	}
    public void selectProjectFilter(String projectType) throws InterruptedException{
    	
    	  /** function to apply priority type filter */
    	
    	  driver.findElement(By.xpath("//*[@id='content']/div[1]/div[4]/div/form/div[1]/div[1]/div[1]/div[1]/div/div[1]/ul/li[1]/button")).click();
    	  driver.findElement(By.id("searcher-pid-input")).sendKeys(projectType);
    	  driver.findElement(By.xpath("//*[@id='searcher-pid-suggestions']/div/ul/li")).click();
    	  Thread.sleep(6000);
    }
    
  public void setIssueTypeFilter(String issueType) throws InterruptedException{
	  
	  /** function to apply issue type filter */
	  
	  driver.findElement(By.xpath("//*[@id='content']/div[1]/div[4]/div/form/div[1]/div[1]/div[1]/div[1]/div/div[1]/ul/li[2]")).click();
	  driver.findElement(By.id("searcher-type-input")).sendKeys(issueType);
	  driver.findElement(By.xpath("//*[@id='searcher-type-suggestions']/div/ul/li")).click();
	  Thread.sleep(6000);
    }

  public void setStatusTypeFilter(String statusType) throws InterruptedException{
	  
	  /** function to apply status type filter */
	  
	  driver.findElement(By.xpath("//*[@id='content']/div[1]/div[4]/div/form/div[1]/div[1]/div[1]/div[1]/div/div[1]/ul/li[3]/button")).click();
	  driver.findElement(By.id("searcher-status-input")).sendKeys(statusType);
	  driver.findElement(By.xpath("//*[@id='searcher-status-suggestions']/div/ul/li")).click();
	  Thread.sleep(6000);
  }
  
  public void selectAssignTypeFilter(String assignType) throws InterruptedException{  
	  
	  /** function to apply assign type filter */
	  
	  driver.findElement(By.xpath("//*[@id='content']/div[1]/div[4]/div/form/div[1]/div[1]/div[1]/div[1]/div/div[1]/ul/li[4]/button")).click();
	  driver.findElement(By.id("assignee-input")).sendKeys(assignType);
	  driver.findElement(By.xpath("//*[@id='assignee-suggestions']/div/ul/li")).click();
	  Thread.sleep(6000);
  }
	public JiraSearch applyFilters() throws InterruptedException{          /** function to search via jira search */
		selectProjectFilter("tst");
		setIssueTypeFilter("bug");
		setStatusTypeFilter("open");
		selectAssignTypeFilter("unassigned");
		driver.findElement(By.xpath("//*[@id='content']/div[1]/div[4]/div/form/div[1]/div[1]/div[1]/div[1]/div/div[1]/ul/li[7]/button")).click();
		return new JiraSearch(driver);
	}
}
