package testproject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
public class testrun {
	WebDriver driver;
	
	@Before
	public void testSetup() {
		driver = new FirefoxDriver();
	}
	@After
	public void testShutDown() {
		driver.close();
	}
	@Test
	public void shouldTestRun() throws InterruptedException {
		HomePage onHomePage = new HomePage(driver);
		onHomePage = onHomePage.navigateToWebApp();                                                                     //go to given test Url
		LoginPage onLoginPage = onHomePage.clickonLogin();                                                             //On given Url page click on login
		DashBoardPage onDashBordPage = onLoginPage.fillFormWithData("enter username", "enter password").getPage();             //On login page fill username and password and go to dash-board
		DashBoardPage openModal = onDashBordPage.getCreateModal();                                                   //On dash-board page click on create issue and get modal
		DashBoardPage createTask = openModal.fillCreateTaskFrom();                                                  // fill create issue form and create issue
		IssuePage issueDetails = onDashBordPage.gotoIssueDetailPage("enter issue id");                                             // on dash-board page enter issue ticket-id on search box and go to issue details page
		IssuePage editIssue = issueDetails.clickEditIssue();                                                      // On Issue details page click on Edit button to edit and get edit issue form
		IssuePage updateIssueDetails  =  editIssue.updateIssue(onDashBordPage);                                  // update some fields and click on update issue
		JiraSearch findIssue = onDashBordPage.searchJiraIssue();                                                // on dash-board page click on issue and go to jira search
		JiraSearch serachIssue  = findIssue.applyFilters();                                                    // add appropriate filter to search existing issues
		
	}
}
