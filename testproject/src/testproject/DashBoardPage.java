package testproject;

import org.apache.bcel.generic.RETURN;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class DashBoardPage extends AbstractPage {

	public DashBoardPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
    
	public DashBoardPage getCreateModal() throws InterruptedException{
		                                                                                               /** open create issue modal */
		
		driver.findElement(By.id("create_link")).click();
		 Thread.sleep(3000);
		 return new DashBoardPage(driver);
	}
	
	public void selectSecurityType(int index) throws InterruptedException{                          /** select security type in create issue form */
		
		Select issueType = new Select(driver.findElement(By.id("security")));
		issueType.selectByIndex(index);
		Thread.sleep(5000);
	}
	
	public void writeSummary(String summary) throws InterruptedException{                            /** select write issue summary in create issue form */
		driver.findElement(By.id("summary")).sendKeys(summary);
		Thread.sleep(1000);
	}
	
	public void selectIssueType(String issue) throws InterruptedException{                           /** select issue type in create issue form */
		driver.findElement(By.id("issuetype-field")).click();
		driver.findElement(By.className("aui-list-item-li-"+issue)).click();
		Thread.sleep(3000);
	}
	
	public void selectPriority(String priority) throws InterruptedException{                        /** select priority type in create issue form */
		driver.findElement(By.id("priority-field")).click();
		Thread.sleep(3000);
		driver.findElement(By.className("aui-list-item-li-"+priority)).click();
		Thread.sleep(2000);
	}
	
	public void selectComponentType(String component) throws InterruptedException{                 /** select component type in create issue form */
		driver.findElement(By.id("components-textarea")).sendKeys("c");
		Thread.sleep(3000);
		driver.findElement(By.className("aui-list-item-li-"+component)).click();
		Thread.sleep(1000);
	}
	
	public void enterEnvironment(String environment) throws InterruptedException{                  /**  write issue environment in create issue form */
		driver.findElement(By.id("environment")).sendKeys(environment);
		Thread.sleep(1000);
	}
	
	public void enterTimeForTask(String time) throws InterruptedException{                         /** time to complete issue in create issue form */
		driver.findElement(By.id("timetracking")).sendKeys(time);
		Thread.sleep(1000);
	}
	
	public void enterDescription(String description) throws InterruptedException{                 /**  write issue description in create issue form */
		driver.findElement(By.id("description")).sendKeys(description);
		Thread.sleep(1000);
	}
	
	public void createIssue() throws InterruptedException{                                      /** click on sumbit button in create issue form */
		driver.findElement(By.id("create-issue-submit")).click();
		Thread.sleep(10000);
	}
	
	public DashBoardPage fillCreateTaskFrom() throws InterruptedException{                  /** Function to create issue */
		selectIssueType("bug");
		writeSummary("This is a test");
		selectSecurityType(2);
		selectPriority("blocker");
		selectComponentType("component-2");
		enterEnvironment("yes good");
		enterTimeForTask("1d");
		enterDescription("test automation for jira");
		createIssue();
		return new DashBoardPage(driver);
	}
	
	public void searchIssue(String issueId){                                                     /** function to type issue ticket id on search box*/
		driver.findElement(By.id("quickSearchInput")).sendKeys(issueId);
	}
	
	public IssuePage gotoIssueDetailPage(String issueId) throws InterruptedException{                          /** function to search existing issue */
		driver.findElement(By.id("quickSearchInput")).click();
		searchIssue(issueId);
		driver.findElement(By.id("quickSearchInput")).sendKeys(Keys.ENTER);
		Thread.sleep(20000);
		return new IssuePage(driver);
	}
	
	public JiraSearch searchJiraIssue() throws InterruptedException{                            /** function to search exiting issue via jira search */
		driver.findElement(By.id("find_link")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id='issues_new_search_link_lnk']")).sendKeys(Keys.ENTER);
		Thread.sleep(10000);
		return new JiraSearch(driver);
	}
	
}
