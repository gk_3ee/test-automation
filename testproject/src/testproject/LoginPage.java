package testproject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends AbstractPage{
	public LoginPage(WebDriver driver) {
		super(driver);
	}
  public LoginPage fillFormWithData(String username, String password) {
	  //On login page enter username and password
	  
	  driver.findElement(By.id("username")).sendKeys(username);
	  driver.findElement(By.id("password")).sendKeys(password);
	  driver.findElement(By.id("rememberMe")).click();
	  
	  return new LoginPage(driver);
  }
  public DashBoardPage getPage() throws InterruptedException{ 
	  //After login got to dash-board
	 
	  driver.findElement(By.id("login-submit")).click();
	  Thread.sleep(10000);
	  return new DashBoardPage(driver);
  }
}
