package testproject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends AbstractPage {
	public HomePage(WebDriver driver){
		super(driver);
		
	}
	public LoginPage clickonLogin() {
		driver.findElement(By.id("user-options")).click();
		return new LoginPage(driver);
	}
}